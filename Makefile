VERSION = 20210214
NAME = zuepfe-keyring-${VERSION}
ARCHIVE = ${NAME}.tar.gz
SIG = ${NAME}.tar.gz.sig
PREFIX ?= /usr/local

all:
	cat zuepfe/* >zuepfe.gpg

install:
	install -dm0755 ${DESTDIR}${PREFIX}/share/pacman/keyrings/
	install -m0644 zuepfe.gpg ${DESTDIR}${PREFIX}/share/pacman/keyrings/
	install -m0644 zuepfe-trusted ${DESTDIR}${PREFIX}/share/pacman/keyrings/
	install -m0644 zuepfe-revoked ${DESTDIR}${PREFIX}/share/pacman/keyrings/

archive:
	git archive --format=tar --prefix=${NAME}/ ${VERSION} | gzip -9 >${ARCHIVE}
	gpg --detach-sign ${ARCHIVE}

upload:
	ssh archlinux mkdir -p web/src/zuepfe-keyring
	scp ${ARCHIVE} ${SIG} archlinux:web/src/zuepfe-keyring/

.PHONY: all install archive upload
